var XLSX = require('xlsx')
const download = require('image-downloader');
const fs = require('fs');

var workbook = XLSX.readFile('Master.xlsx');
var sheet_name_list = workbook.SheetNames;
var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

let startSku = process.argv[2];
let endSku = process.argv[3];
let getItem = false;
if(!startSku || startSku == "false") getItem = true;

let startIndex = 0;
let endIndex = 20;

syncData(startIndex, endIndex);

function syncData(start, end) {
  if(xlData.slice(start, end).length == 0) return;

  xlData.slice(start, end).forEach(async item => {
    if(item.sku == startSku) getItem = true;
    if(getItem) {
      if(item.sku == endSku) getItem = false;

      await downloadImages(item);
    }
  });

  setTimeout(() => {
    syncData(end + 1, end + endIndex);
  }, 10000)
}

function downloadImages(item) {
  Object.keys(item).reduce((index, key) => {
    if(key.startsWith("images")) {
      let imageName = index == 0 ? `images/${item.sku}.jpg` : `images/${item.sku}_${index}.jpg`;
      console.log(`---- Start download image sku ${item.sku} ----- key: ${key} --- url: ${imageName}`)
      download.image({ url: item[key], dest: imageName }).catch(function () {
        console.log(`----- sku ${item.sku} have url: ${imageName} error ------`);
        fs.appendFile('images/logs.txt', `----- sku ${item.sku} have url: ${imageName} error ------\n`, function (err) {
          if (err) throw err;
        });
      });
      console.log(`---- End download image sku ${item.sku} ----- key: ${key}`)
      index++;
    }

    return index;
  }, 0)
}
