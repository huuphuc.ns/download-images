## Getting started

Install project
```
Git clone https://gitlab.com/huuphuc.ns/download-images.git
cd download-images
yarn install
```

## Command download all images

```
node index.js startSku endSku
```

- startSku là sku start down ảnh (exp: MXMA00000099). Có để k truyền or truyền false nếu download từ sku đầu tiên.

```
node index.js
node index.js false MXMA00000101
node index.js MXMA00000099 MXMA00000101
```

- endSku là sku kết thúc down ảnh (exp: MXMA00000101). Có thể k truyền nếu muốn kêt thúc là sku cuối cùng.

```
node index.js
node index.js MXMA00000099
node index.js MXMA00000099 MXMA00000101
```
